const fs = require("fs");
const _ = require("underscore");
const X2JS = require("x2js");
let fulldienststellen = JSON.parse(fs.readFileSync("dienststellen.json"));

for (const dst of fulldienststellen) {
  for (let lindex = 0; lindex < dst.leader.length; lindex++) {
    const leader = dst.leader[lindex];
    leader.ov = dst.code;
  }

  if (dst.units) {
    for (let index = 0; index < dst.units.length; index++) {
      const unit = dst.units[index];
      unit.code = `${dst.code}-${index}`;
      unit.ov = dst.code;
      for (let i2 = 0; i2 < unit.teileinheit.length; i2++) {
        const teilunit = unit.teileinheit[i2];
        teilunit.code = `${dst.code}-${index}-${i2}`;
        teilunit.parent = `${dst.code}-${index}`;
        teilunit.ov = dst.code;
      }
    }
  }
}

function renameBadKeyNames(o) {
  var newName = "";
  for (var i in o) {
    if (o[i] !== null && typeof o[i] == "object") {
      //going one step down in the object tree!!
      renameBadKeyNames(o[i]);
    }
    newName = i.trim().replace(/\s+/g, "_");
    if (i != newName) {
      if (o.hasOwnProperty(i)) {
        o[newName] = o[i];
        delete o[i];
      }
    }
  }
}

function fixObjectArray(o) {
  var names = ["row", "entry", "key", "item", "node"];
  var s = JSON.stringify(o, null, "\t");
  var a = s.split(/\r\n|\n|\r/gm);
  var c, i, j, k, x, z;

  function isArrayNoName(text, stop) {
    var count = 0;
    var index = 0;
    while (text.charAt(index) === "\t") {
      count++;
      index++;
    }
    if (text.charAt(index) === stop) return count;
    else return -1;
  }
  for (j = 0; j < a.length; j++) {
    x = isArrayNoName(a[j], "[");
    if (x >= 0) {
      if (a[j].substring(j) === "[]") {
        a[j] = '{ "' + names[x % names.length] + '": ' + a[j] + "}";
        continue;
      }
      if (a[j].substring(j) === "[],") {
        a[j] =
          '{ "' +
          names[x % names.length] +
          '": ' +
          a[j].substring(0, a[j].length - 1) +
          "},";
        continue;
      }
      a[j] = '{ "' + names[x % names.length] + '": ' + a[j];
      for (k = j + 1; k < a.length; k++) {
        c = isArrayNoName(a[k], "]");
        if (c === x) {
          z = a[k].slice(-1);
          if (z === ",") {
            a[k] = a[k].substring(0, a[k].length - 1) + "},";
          } else {
            a[k] += "}";
          }
          break;
        }
      }
    }
  }
  return a.join("\n");
}

function runit(jsobj) {
  var x2js = new X2JS();
  var xml = '<?xml version="1.0" encoding="UTF-8" ?>\n';
  try {
    obj = jsobj;
    if (!obj) {
      console.log("Your JSON is not valid.");
      return;
    }
    renameBadKeyNames(obj);

    if (
      !_.isArray(obj) &&
      (_.keys(obj).length > 1 ||
        (_.keys(obj).length == 1 && _.isArray(obj[_.keys(obj)[0]])))
    ) {
      obj = { root: obj }; // object with multiple properties or one property of type array
    } else if (!_.isArray(obj) && _.keys(obj).length == 0) {
      obj = { root: obj }; // empty object
    } else if (_.isArray(obj)) obj = { root: { row: obj } }; // If an array of objects do this

    if (obj) {
      s = fixObjectArray(obj);
      obj = JSON.parse(s);
    }

    //if(obj) {
    //   Object.keys(obj).forEach(function(key,index) {
    //      if(_.isObject(obj[key]))obj[key].renameProperty(key,key.trim().replace(/\s+/g,"_"));
    //   });
    // }
    s = x2js
      .js2xml(obj)
      .replace(/\<-/gm, "<")
      .replace(/\<\/-/gm, "</")
      .replace(/\<#/gm, "<")
      .replace(/\<\/#/gm, "</");
    return xml + s;
  } catch (e) {
    console.log(
      "Invalid JSON entered " + (e.Description ? ":" + e.Description : ""),
      e
    );
  }
}
fs.writeFileSync("with_uid.json",JSON.stringify(fulldienststellen));
fs.writeFileSync("with_uid.xml", runit(fulldienststellen));
