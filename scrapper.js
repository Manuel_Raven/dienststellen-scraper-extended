// Modules
const cheerio = require('cheerio');
const fetch = require('node-fetch');
const fs = require('fs');
// Helper Functions
//#region GlobalVariables
let FIRST_URL = "https://www.thw.de/SiteGlobals/Forms/Suche/DE/ThwInIhrerNaehe_Startseite_Formular.html"
let DATA_URL = "https://www.thw.de/DE/THW/Bundesanstalt/Dienststellen/dienststellen_node.html?oe_plzort=PLZ+oder+Ort&umkreissuche=1&sorting=cityasc&page=1&resultsPerPage=1000&tabreiter=1&oe_umkreis=2000"
let request_headers = {
    'Accept': 'text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8',
    'Accept-Language': 'en-US,en;q=0.5',
    'Connection': 'keep-alive'
}

let REPO_UPSTREAM = "https://${TOKEN_NAME}:${TOKEN_SECRET}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}"
let FILE_NAME = "address_data.json"
let oucodes = JSON.parse(fs.readFileSync('ou_code.json'))
let OSM_DATA = {}
//#endregion GlobalVariables

//#region helperfunctions
function Sleep(milliseconds) {
    return new Promise(resolve => setTimeout(resolve, milliseconds));
}
function parseCookies(response) {
    const raw = response.headers.raw()['set-cookie'];
    return raw.map((entry) => {
        const parts = entry.split(';');
        const cookiePart = parts[0];
        return cookiePart;
    }).join(';');
}
function RemoveSessionLink(link) {
    return link ? link.split(';')[0] : ""
}
//#endregion helperfunctions

//#region Extractor
async function ScrapContact($) {
    let tempobj = { street: "", code: "", oeId: "", website: "", zip: "", city: "", region: "", phone: "", fax: "", email: "", takzeich: "", name: "", coordinates: [], leader: [], osm_data: {} }
    tempobj.street = $('.street-address').text();
    tempobj.zip = $('.postal-code').text().trim();
    tempobj.city = $('.locality').text().trim();
    tempobj.region = $('.region').text();
    tempobj.name = $('.isFirstInSlot').text();
    tempobj.code = oucodes[tempobj.name] || ""
    let raw_osm_data = OSM_DATA.elements.find(x => { if (x.tags && x.tags['ref:thw']) { return x.tags['ref:thw'] == tempobj.code } else { return false } }) || null
    if (raw_osm_data) {

        tempobj.osm_data['coordinates'] = [raw_osm_data.lat || raw_osm_data.center.lat, raw_osm_data.lon || raw_osm_data.center.lon]
        tempobj.osm_data['osm_id'] = raw_osm_data['id'] || ''
        tempobj.osm_data['osm_type'] = raw_osm_data['type'] || ''
        tempobj.osm_data['housenumber'] = raw_osm_data.tags['addr:housenumber'] || ''
        tempobj.osm_data['postcode'] = raw_osm_data.tags['addr:postcode'] || ''
        tempobj.osm_data['street'] = raw_osm_data.tags['addr:street'] || ''
        tempobj.osm_data['email'] = raw_osm_data.tags['contact:email'] || ''
        tempobj.osm_data['fax'] = raw_osm_data.tags['contact:fax'] || ''
        tempobj.osm_data['phone'] = raw_osm_data.tags['contact:phone'] || ''
        tempobj.osm_data['website'] = raw_osm_data.tags['contact:website'] || ''
        console.log("Adding OSM Data")
    }
    // Contact Data

    tempobj.phone = $('em:contains("Tel:")').parent().children('.value').text().replace('0', '+49 ');
    tempobj.fax = $('em:contains("Fax:")').parent().children('.value').text().replace('0', '+49 ');
    tempobj.email = $('span:contains("E-Mail:")').parent().children('.value').children('a').attr('href').replace('mailto:', '');;
    tempobj.oeId = parseInt(($('#servicemaplink').attr("href").split('?'))[1].replace('oeId=', ''))
    tempobj.coordinates = await ExtractLocation($('#servicemaplink').attr("href"));
    tempobj.takzeich = RemoveSessionLink($('.contact').children('.vcard').children('img').attr('src'));
    tempobj.website = $('.url').children('span').children('a').attr('href') || ""


    // Leader

    let leadercards = $('.box.personenBox').find('.vcard');
    //console.log(leadercards.find(".vcard").html());

    for (const leadercard of leadercards) {
        let templeader = { name: "", picture: "", title: "" }
        templeader.picture = RemoveSessionLink($(leadercard).find('img').attr('src'));
        templeader.name = $(leadercard).find('.fn.n').children('.given-name').text() + " " + $(leadercard).find('.fn.n').children('.family-name').text();
        templeader.title = $(leadercard).find('.title').text();
        tempobj.leader.push(templeader);
    }
    return tempobj;

}

async function ExtractParent($) {
    let sub = $('h3:contains("Übergeordnete Dienststelle")');
    let childs = sub.parent();
    let UL = childs.children('ul');
    let allLI = UL.find('li');
    let parents = [];
    for (const subLI of allLI) {

        parents.push($(subLI).children('a').text());
    }
    return parents;
}

async function ExtractLocation(link) {
    var url = `${link}`;
    try {
        await Sleep(200);
        const response = await fetch(url);
        const body = await response.text();
        const lines = body.split(/\r?\n/);
        let lat = 0;
        let lng = 0;
        for (const line of lines) {
            if (line.includes('lat = parseFloat')) {
                lat = parseFloat(line.split("(")[1].split(")")[0])
            } else if (line.includes('lng = parseFloat')) {
                lng = parseFloat(line.split("(")[1].split(")")[0])
            }
        }
        return [lat, lng];
    }
    catch (error) {
        console.log("Location: " + error + " " + link);
    }
}

async function ExtractUnits($) {
    let unitarray = [];
    let sub = $('h2:contains("Einheiten")');
    let childs = sub.parent();
    let UL = childs.children('ul');
    let allLI = UL.children('li').children('ol').children('li');
    for (const subLI of allLI) {
        let tempunit = { name: "", link: "", teileinheit: [] }
        tempunit.name = $(subLI).children('div').children('h3').text();
        tempunit.link = RemoveSessionLink($(subLI).children('div').children('span').children('a').attr('href'));

        let teillis = $(subLI).children('ul').children('li')
        for (const teilli of teillis) {
            let tempteileinheit = { name: "", link: "", extra: "" }
            tempteileinheit.name = $(teilli).children('div').find('h3').text() + $(teilli).children('div').find('h4').text();
            tempteileinheit.link = RemoveSessionLink($(teilli).children('div').find('a').attr('href'));

            // If the name start with 1. or 2. remove it
            if (tempteileinheit.name.startsWith("1.") || tempteileinheit.name.startsWith("2.")) {
                tempteileinheit.name = tempteileinheit.name.substring(3);
            }

            // Oelschaden Workaround
            let next = $(teilli).next();
            if (next.length > 0) {
                if (next[0].name == "ul") {
                    tempteileinheit.extra = ($(next).text()).trim();
                }
            }
            else {
                tempteileinheit.extra = ($(teilli).children('ul').text()).trim();
            }
            
            tempunit.teileinheit.push(tempteileinheit);
        }

        unitarray.push(tempunit);
    }
    return unitarray
}
//#endregion Extractor


//#region Main
async function GetDienststellenList() {
    try {
        //Get Cookies
        let body = ""
        if (fs.existsSync('website.html')) {
            body = fs.readFileSync('website.html');
        } else {
            const cookieresponse = await fetch(FIRST_URL, { headers: request_headers });
            let cookies = parseCookies(cookieresponse);
            // Get DATA
            let combinedheaders = { ...{ 'cookie': cookies }, ...request_headers }
            console.log(combinedheaders)
            const response = await fetch(DATA_URL, { headers: combinedheaders });
            body = await response.text();
        }

        let $ = cheerio.load(body);
        let adress_table = $('table.address-list tbody').children('tr.zeileGerade, tr.zeileUngerade')
        let dienstellenlist = []
        for (const row of adress_table) {
            dienstellenlist.push($(row).find('a').attr('href'))
        }
        return dienstellenlist
    }
    catch (error) {
        console.log(error);
    }
}

async function ScrapDienststelle(link) {
    var success = false;
    while (!success) {
        success = true;
        var url = `https://www.thw.de/${link}`;
        try {
            console.log("Try fetch", url)
            const response = await fetch(url, { timeout: 5000 });
            const body = await response.text();
            let $ = cheerio.load(body);
            // Decide department type by URL
            let contactdata = await ScrapContact($);
            let type = ""
            if (contactdata.name.includes('Ortsverband')) {
                type = "OV"
            } else if (contactdata.name.includes('Regionalstelle')) {
                type = "RST"
            } else if (contactdata.name.includes('Landesverband')) {
                type = "LV"
            } else if (contactdata.name.includes('Leitung')) {
                type = "LT"
            } else if (contactdata.name.includes('Ausbildungszentrum')) {
                type = "AZ"
            }
            let parents = await ExtractParent($);
            let tempobj = { link, type, parents }
            let localobj = { ...contactdata, ...tempobj }
            if (type == "OV") {
                let units = await ExtractUnits($)
                localobj.units = units
            }
            //console.log(localobj)
            return localobj
        }
        catch (error) {
            console.log(error);
            success = false;
            console.log("Retry")
        }
    }
}


async function GetOSMData() {
    console.log("Get Full Data From Overpass API")
    const response = await fetch('https://overpass-api.de/api/interpreter?data=%5Bout%3Ajson%5D%5Btimeout%3A90%5D%3B%28node%5B%22emergency%5Fservice%22%3D%22technical%22%5D%5B%22operator%22%3D%22Bundesanstalt%20Technisches%20Hilfswerk%20%28THW%29%22%5D%2847%2E212105775622%2C5%2E712890625%2C55%2E040614327717%2C15%2E084228515625%29%3Bway%5B%22emergency%5Fservice%22%3D%22technical%22%5D%5B%22operator%22%3D%22Bundesanstalt%20Technisches%20Hilfswerk%20%28THW%29%22%5D%2847%2E212105775622%2C5%2E712890625%2C55%2E040614327717%2C15%2E084228515625%29%3Brelation%5B%22emergency%5Fservice%22%3D%22technical%22%5D%5B%22operator%22%3D%22Bundesanstalt%20Technisches%20Hilfswerk%20%28THW%29%22%5D%2847%2E212105775622%2C5%2E712890625%2C55%2E040614327717%2C15%2E084228515625%29%3B%29%3Bout%20center%3B%3E%3Bout%20skel%20qt%3B%0A', { timeout: 90000 })
    OSM_DATA = await response.json()
    return;
}


async function Main() {
    await GetOSMData()
    let dienststellen = await GetDienststellenList();
    let fulldienststellen = []
    let current = 0;
    for (const dienststelle of dienststellen) {
        let scrapedDienststelle = await ScrapDienststelle(dienststelle);
        current++
        console.log(`Scrapped ${current}/${dienststellen.length}`)
        fulldienststellen.push(scrapedDienststelle);
    }
    console.log(fulldienststellen);
    fs.writeFileSync('dienststellen.json', JSON.stringify(fulldienststellen))
}
Main();


// async function TestMain() {
//     var url = `https://thw.de/SharedDocs/Organisationseinheiten/DE/Ortsverbaende/B/Borna_Ortsverband.html?nn=922532`;
//     try {
//         console.log("Try fetch", url)
//         const response = await fetch(url, { timeout: 5000 });
//         const body = await response.text();
//         let $ = cheerio.load(body);
//         let data = await ExtractUnits($);
//         console.log(JSON.stringify(data, null, 2))
//     }
//     catch (error) {
//         console.log(error);
//     }
// }
// TestMain()
//#endregion Main