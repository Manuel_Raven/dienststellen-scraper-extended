# Daten der THW Dienststellen - Extended

Dieses Projekt ist ein Klon des Projekts ['thw-dienststellen'](https://git.ovcms.thw.de/git-api-aggregation/thw-dienststellen).
Es basiert auf der gleichen Technisches Grundlage ist aber in NodeJS anstatt in Python geschrieben.
Zusätzlich werden die Datensätze mit folgenden ***öffentlich verfügbaren*** Information angereichert:

- type (OV,RST,LV,LT,AZ)
- Link (Referenz wo auf thw.de)
- region (Bundesland)
- leader (Führungskräfte der Dienststelle)
- takzeich (Link zum angegebenen Taktischen Zeichen der Dienststelle)
- osm_data (Export von Hinterlegten Daten aus OSM via Public Overpass API, referenziert via OU-Code und OSM-Tag 'ref:thw')
- Bei OV: units (Einheiten und Teileinheiten des Orstverbandes)

Ansonsten sind alle anderen Informationen identisch wie bei ['thw-dienststellen'](https://git.ovcms.thw.de/git-api-aggregation/thw-dienststellen):

```json
[
{
        "street": "Eckenerstraße 52",
        "code": "OAAC",
        "oeId": 2000612,
        "website": "http://www.thw-aachen.de",
        "zip": "52078",
        "city": "Aachen",
        "region": "Nordrhein-Westfalen",
        "phone": "+49 241 515667 0",
        "fax": "+49 241 515667 18",
        "email": "OV-Aachen@thw.de",
        "takzeich": "/SharedDocs/Bilder/DE/TiUe/TaktischeZeichen/Einheit/OV.png",
        "name": "Ortsverband Aachen",
        "coordinates": [
            50.756488,
            6.158488
        ],
        "leader": [
            {
                "name": "Albert Willekens",
                "picture": "/SharedDocs/Bilder/DE/TiUe/NoElementPerson.jpg",
                "title": "Ortsbeauftragte/r"
            }
        ],
        "osm_data": {
            "coordinates": [
                50.7573855,
                6.1592662
            ],
            "housenumber": "52",
            "postcode": "52078",
            "street": "Eckenerstraße",
            "email": "ov-aachen@thw.de",
            "fax": "+49 241 51566718",
            "phone": "+49 241 5156670",
            "website": "http://www.thw-aachen.de/"
        },
        "link": "SharedDocs/Organisationseinheiten/DE/Ortsverbaende/A/Aachen_Ortsverband.html?nn=922532",
        "type": "OV",
        "parents": [
            "Landesverband Nordrhein-Westfalen",
            "Regionalstelle Aachen"
        ],
        "units": [
            {
                "name": "Fachzug Logistik",
                "link": "",
                "teileinheit": [
                    {
                        "name": "Zugtrupp Fachzug Logistik",
                        "link": "SharedDocs/Einheiten/DE/Inland/ZTr-FZ-Log.html",
                        "extra": ""
                    },
                    {
                        "name": "Fachgruppe Logistik-Materialwirtschaft",
                        "link": "SharedDocs/Einheiten/DE/Inland/FGr-Log-MW.html",
                        "extra": ""
                    },
                    {
                        "name": "Fachgruppe Logistik-Verpflegung",
                        "link": "SharedDocs/Einheiten/DE/Inland/FGr-Log-V.html",
                        "extra": ""
                    }
                ]
            },
            {
                "name": "1. Technischer Zug",
                "link": "SharedDocs/Einheiten/DE/Inland/TZ.html",
                "teileinheit": [
                    {
                        "name": "Zugtrupp",
                        "link": "SharedDocs/Einheiten/DE/Inland/ZTr-TZ.html",
                        "extra": ""
                    },
                    {
                        "name": "Bergungsgruppe",
                        "link": "SharedDocs/Einheiten/DE/Inland/B.html",
                        "extra": "Einsatz-Gerüst-System (EGS)"
                    },
                    {
                        "name": "Fachgruppe Notversorgung und Notinstandsetzung",
                        "link": "SharedDocs/Einheiten/DE/Inland/FGr-N.html",
                        "extra": ""
                    },
                    {
                        "name": "Fachgruppe Elektroversorgung",
                        "link": "SharedDocs/Einheiten/DE/Inland/FGr-E.html",
                        "extra": ""
                    }
                ]
            },
            {
                "name": "2. Technischer Zug",
                "link": "SharedDocs/Einheiten/DE/Inland/TZ.html",
                "teileinheit": [
                    {
                        "name": "Zugtrupp",
                        "link": "SharedDocs/Einheiten/DE/Inland/ZTr-TZ.html",
                        "extra": ""
                    },
                    {
                        "name": "Bergungsgruppe",
                        "link": "SharedDocs/Einheiten/DE/Inland/B.html",
                        "extra": ""
                    },
                    {
                        "name": "Fachgruppe Ortung (A)",
                        "link": "SharedDocs/Einheiten/DE/Inland/FGr-O-A.html",
                        "extra": ""
                    },
                    {
                        "name": "Fachgruppe Schwere Bergung (a)",
                        "link": "SharedDocs/Einheiten/DE/Inland/FGr-SB-A.html",
                        "extra": ""
                    }
                ]
            }
        ]
    },
  ...
]
```

## Export und Aktualisierung

Ähnlich wie bei ['thw-dienststellen'](https://git.ovcms.thw.de/git-api-aggregation/thw-dienststellen) werden die Daten regelmäßig (wöchentlich) von thw.de gescraped und hier bereitgestellt.

Das OU-Mapping [ou_code.json](ou_code.json) ist eine 1:1 Kopie von ['thw-dienststellen'](https://git.ovcms.thw.de/git-api-aggregation/thw-dienststellen). Diese basiert wie dort angegeben auf diesen: [Open-Data Datensatz](https://www.thw.de/SharedDocs/Downloads/DE/Open-Data/thw_adressen_mit_geoinformationen_csv.html)

