// Modules
const cheerio = require("cheerio");
const fetch = require("node-fetch");
const fs = require("fs");
// Helper Functions
//#region GlobalVariables
let FIRST_URL =
  "https://www.thw.de/DE/Einheiten-Technik/Fachgruppen/fachgruppen_node.html";
let DATA_URL =
  "https://www.thw.de/DE/Einheiten-Technik/Fachgruppen/fachgruppen_node.html";
let request_headers = {
  Accept:
    "text/html,application/xhtml+xml,application/xml;q=0.9,image/webp,*/*;q=0.8",
  "Accept-Language": "en-US,en;q=0.5",
  Connection: "keep-alive",
};

let REPO_UPSTREAM =
  "https://${TOKEN_NAME}:${TOKEN_SECRET}@${CI_SERVER_HOST}/${CI_PROJECT_PATH}";
//#endregion GlobalVariables

//#region helperfunctions
function Sleep(milliseconds) {
  return new Promise((resolve) => setTimeout(resolve, milliseconds));
}
function parseCookies(response) {
  const raw = response.headers.raw()["set-cookie"];
  return raw
    .map((entry) => {
      const parts = entry.split(";");
      const cookiePart = parts[0];
      return cookiePart;
    })
    .join(";");
}
function RemoveSessionLink(link) {
    let nosession = link ? link.split(";")[0] : "";
    return nosession ? nosession.split("?")[0] : "";
  }
//#endregion helperfunctions

//#region Extractor

async function GetFachgruppenList() {
  let combinedheaders = null;
  if (fs.existsSync("website.html")) {
    body = fs.readFileSync("website.html");
  } else {
    const cookieresponse = await fetch(FIRST_URL, { headers: request_headers });
    let cookies = parseCookies(cookieresponse);
    // Get DATA
    combinedheaders = { ...{ cookie: cookies }, ...request_headers };
    //console.log(combinedheaders);
  }
  try {
    //Get Cookies
    let body = "";
    let fachgruppenlist = [];
    let local_data_url = DATA_URL;
    while (local_data_url != null) {
      console.log("Load", local_data_url);
      const response = await fetch(local_data_url, {
        headers: combinedheaders,
      });
      body = await response.text();
      let $ = cheerio.load(body);
      let fachgruppen_table = $("#main > div > div.teaserlist").children(
        "div.teaser"
      );
      for (const row of fachgruppen_table) {
        fachgruppenlist.push($(row).find("a").attr("href"));
      }
      //console.log(fachgruppenlist);
      let control_buttons = $("#begin > ul.right.presearch").children("li");
      if (control_buttons.length == 1) {
        next_button = control_buttons.find("a");
        console.log(1, next_button.html());
      } else {
        next_button = control_buttons.find("a").last();
        console.log(2, next_button.html());
      }
      local_data_url =
        next_button.html() == "Weiter"
          ? `https://thw.de/${next_button.attr("href")}`
          : null;
      await Sleep(200);
    }
    return fachgruppenlist;
  } catch (error) {
    console.log(error);
  }
}

//#endregion Extractor
async function ExtractFachgruppenData(link) {
  var success = false;
  while (!success) {
    success = true;
    var url = `https://www.thw.de/${link}`;
    try {
      console.log("Try fetch", url);
      const response = await fetch(url, { timeout: 5000 });
      const body = await response.text();
      let $ = cheerio.load(body);
      // Decide department type by URL
      let tempobj = {
        name: "",
        link: RemoveSessionLink(link),
        short: "",
        shortdesc: "",
        takzeich: "",
        textfragments: [],
        tasks: [],
      };
      tempobj.name = $(".isFirstInSlot").text();
      try {
        let splitted = tempobj.name.split("(");
        splitted.shift();
        let preshort = "(" + splitted.join("(");
        let secondshortsplitted = preshort.split(") (");
        if (secondshortsplitted.length > 1) {
          tempobj.short = secondshortsplitted[1].substring(
            0,
            secondshortsplitted[1].length - 1
          );
        } else {
          tempobj.short = secondshortsplitted[0]
            .substring(0, secondshortsplitted[0].length - 1)
            .substring(1);
        }
      } catch {}
      tempobj.shortdesc = $(".abstract").text();
      tempobj.takzeich = RemoveSessionLink(
        $(".unit").children("li").children("img").attr("src")
      );
      pfrags = $(".photogallery").children("p");
      for (const pfrag of pfrags) {
        let fragtext = $(pfrag).text();
        if (!fragtext.includes("Zu den Aufgaben")) {
          tempobj.textfragments.push(fragtext);
        }
      }
      let tasktables = $(".photogallery").children("ul").last().children("li");
      for (const task of tasktables) {
        tempobj.tasks.push($(task).text());
      }
      return tempobj;
    } catch (error) {
      console.log(error);
      success = false;
      console.log("Retry");
    }
  }
}
//#region Main

async function Main() {
  let fachgruppen = await GetFachgruppenList();
  let fachgruppen_data = [];
  console.log(fachgruppen);
  for (const fachgruppe of fachgruppen) {
    let data = await ExtractFachgruppenData(fachgruppe);
    //console.log(data);
    await Sleep(300);
    fachgruppen_data.push(data);
  }
  fs.writeFileSync("units.json", JSON.stringify(fachgruppen_data));
}
Main();
//#endregion Main
